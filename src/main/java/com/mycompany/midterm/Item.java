package com.mycompany.midterm;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ModG
 */
public class Item implements Serializable{
    private String ID;
    private String Name;
    private String Brand;
    private int Price;
    private int Amount;

    

    public Item(String ID, String Name, String Brand, int Price, int Amount) {
        this.ID = ID;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = Amount;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }@Override
    public String toString() {
        return "Name{" + "ID=" + ID + ", Name=" + Name + ", Brand=" + Brand + ", Price=" + Price + ", Amount=" + Amount + '}';
    }
}
