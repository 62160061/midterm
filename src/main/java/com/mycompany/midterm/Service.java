package com.mycompany.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ModG
 */
public class Service {
    private  static ArrayList<Item> userList = new ArrayList<>();
    static  {
        load();
    }
    public static boolean addUser(Item Item){
        userList.add(Item);
        save();
        return true;
    }//delete(D)
    public static boolean deleteUser(Item Item){
        userList.remove(Item);
        save();
        return true;
    }public static boolean deleteUser(int index){
        userList.remove(index);
        save();
        return true;
    }public static boolean clear(){
        userList.clear();
        return true;
    }static int totalItem() {
        int sumtotalItem = 0;

        for (int i = 0; i < userList.size(); i++) {
            sumtotalItem += userList.get(i).getAmount();
        }

        return sumtotalItem;
    }

    static double totalPrice() {

        double sumprice = 0.00;

        for (int i = 0; i < userList.size(); i++) {
            sumprice += userList.get(i).getAmount() * userList.get(i).getPrice();
        }

        return sumprice;
    }
//read(R)
    public static ArrayList<Item> getUsers(){
        return userList;
    }public static Item getUser(int index){
        return userList.get(index);
    }//update(U)
    public static boolean updateUser(int index,Item Item){
        userList.set(index, Item);
        save();
        return true;
    }public static void save(){
        try {
            File file = null;
            FileOutputStream fos =null;
            ObjectOutputStream oos = null;
            file =new File("Bright.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }public static void load(){
        try {
            File file = null;
            FileInputStream fis =null;
            ObjectInputStream ois = null;
            file =new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList= (ArrayList<Item>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
